# 🟢 Status and Notices

---

The latest important ongoing notices, updates or announcements can be found here.

!!! Failure "Vanguard anticheat has been implemented in League of Legends"
    Since patch [14.9](https://www.leagueoflegends.com/en-us/news/game-updates/patch-14-9-notes/), Vanguard has been activated.
    Starting League of Legends returns an Error. League of Legends will not be playable for the forseeable future.


## ❓ Statuses

## :warning: THE BELOW IS NO LONGER MAINTAINED

### League of Legends

Feature | Functionality | Comments | Relevant Pages
----------|-----------------|----------------|---------------
Riot client | ✅ Full |  | 
LoL client | ✅ Full | Minor performance issues or visual bugs | [▶️ How to Install League of Legends](install/index.md), [📈 How to Optimise League of Legends](optimise/index.md)
LoL game | ❌ None | Currently crashing, see notices | [▶️ How to Install League of Legends](install/index.md), [📈 How to Optimise League of Legends](optimise/index.md)
LoL voice | ✅ Full | | 
LoL PBE server | ❌ Game crashing | Currently crashing, see notices | [▶️ How to Install League of Legends](install/index.md)
LoL China servers | ❓Unknown  | Please reach out to /u/TheAcenomad if you have reports of Chinese server performance | 

### Other Games

Game | Functionality | Comments | Relevant Pages
----------|-----------------|----------------|---------------
Teamfight Tactics | ✅ Full | Near native performance | [▶️ How to Install League of Legends](install/index.md), [🕹️ Other Riot Games on Linux](other_games/index.md)
VALORANT | ❌ None | Does not run native, via Wine, or via virtualsiation | [🕹️ Other Riot Games on Linux](other_games/index.md), [Vanguard Anticheat? What is it? Is it Coming to League?](faq/vanguard.md)
Legends of Runeterra |  ✅ Full |  | [▶️ How to Install League of Legends](install/index.md), [🕹️ Other Riot Games on Linux](other_games/index.md)
Ruined King: A League of Legends Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Hextech Mayhem: A League of Legends Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Song of Nunu: A League of Legends Story | ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
CONV/RGENCE: A League of Legends Story |  ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
Project L |  ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
The Mageseeker: A League of Legend Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Song of Nunu: A League of Legends Story | ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
