# How to Write a Support Request Post and Post Template

Writing good support request posts help us better help you! The easier you can make the lives of the volunteers on the forums, the quicker we can help you solve your issue. Generally the rule is: **the more info you can provide in the initial post, the better**. Minor issues might not warrant a wall of text, but usually all posts should contain some essential information, as well as information specific to your issue.

## Essential Information

The following information is **mandatory** on all support request posts. Posts missing this information will be removed.

* Distribution and if applicable, distro version, eg. Ubuntu 22.04, Arch Linux, etc.
* Installation method, eg. Lutris, Snap, leagueoflegends-git, etc.
* Wine version used to play, eg. `Lutris-GE-7.0-8-LoL`

## Issue-specific Information

Depending on the nature of your issue, there may be other information that are also necessary to help diagnose your issues. Use your discretion and remember that **the more information you can provide, the better**:

* Detailed hardware specs: CPU model, GPU model, display resolution and number of displays, etc.
* Detailed software specs: window manager and desktop environment, system Wine version, etc.
* Screenshots 
* If you have already tried solutions: what did you try and what were their outcomes

## Logs

🪵 Logs are important! While not every support post needs a logs pastebin attached, **the vast majority of them do**.

Your post will not be removed if it lacks logs, but please bear in mind that they are often **vital to solving most issues!**

For more information, read [How to Capture Verbose Logs](logs.md).

## Post Template

Copy and paste the below into a new text post, filling in the necessary boxes. If you are unfamiliar with reddit formatting, [check here](https://support.reddithelp.com/hc/en-us/articles/360043033952-Formatting-Guide).

    ### Issue

    [Describe your issue here. Don't just say "it doesn't work", be descriptive!]

    ### Distribution Version
    
    [eg. Ubuntu 22.04, Arch Linux]

    ### Installation Method
    
    [eg. Lutris, Snap, leagueoflegends-git, etc.]

    ### Wine Version
    
    [eg. `Lutris-GE-7.0-8-LoL`]

    ### Logs
    
    [Pastebin link here]

    ### Additional info
    
    [Anything else important here!]
