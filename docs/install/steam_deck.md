# Installing League of Legends on the Steam Deck

![Steam_Deck](../assets/steamdeck.png)

## What is the Steam Deck?

[In 2022](https://en.wikipedia.org/wiki/Steam_Deck) Valve released their Linux-based handheld game system, the [Steam Deck](https://www.steamdeck.com/en/). A key element of the Steam Deck is it's open nature, so open in fact that Valve themselves have an [official teardown video](https://www.youtube.com/watch?v=uTFey3FPAqs) documenting the hardware! With that in mind, it's very much possible to play and run League of Legends on the Deck, even if the native controls aren't well-suited for a MOBA.

## A Note on Disabling Read-Only

Disabling read-only mode is necessary as the Flatpak version of Lutris is not compatible with League of Legends, therefore installation via Pacman is necessary and ergo disabling the read-only system image. Valve [recommends this only for advanced users](https://help.steampowered.com/en/faqs/view/671A-4453-E8D2-323C), and additionally per their documentation note that future SteamOS updates may erase or overwrite packages installed via Pacman, so you may need to repeat the installation steps after a system update.

## How to Install

1. Boot into desktop mode and open a terminal.
1. Disable read-only mode: `sudo steamos-readonly disable`.
3. Follow the steps in the Lutris wiki page on [Installing Drivers](https://github.com/lutris/docs/blob/master/InstallingDrivers.md) for Arch Linux on AMD:
 1. Enable multilib (32-bit support) by uncommenting the `[multilib]` section in `/etc/pacman.conf`
 2. Run a package update, reboot if necessary: `sudo pacman -Syu`.
 3. Install support for Vulkan API: `sudo pacman -S --needed lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader`
4. Install additional Vulkan packages: `sudo pacman -S vulkan-mesa-layers lib32-vulkan-mesa-layers vkd3d lib32-vkd3d`
5. Follow the steps in the Lutris wiki page on [Wine Dependencies](https://github.com/lutris/docs/blob/master/WineDependencies.md) on Arch Linux:
 1. Download all necessary Wine dependencies:
`sudo pacman -S --needed wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls \`
`mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error \`
`lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo \`
`sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \`
`ncurses lib32-ncurses ocl-icd lib32-ocl-icd libxslt lib32-libxslt libva lib32-libva gtk3 \`
`lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader`
6. Install Lutris via Pacman, **not** via Flatpak or Discover: `sudo pacman -S lutris`
7. Open [the main League of Legends Lutris page](https://lutris.net/games/league-of-legends/) in a browser and click install on the launcher titled `Standard version` for your region.
8. Follow the instructions in the launcher popups, please note:
    * Wait for **both** downloads to finish. There are two downloads one after the other: the client download and the game download
    * Do **not** enter your login details and login or click play, instead just quit the launcher when the two downloads have finished
9. Follow the Lutris popups instructions to finish the install
10. Disable the Lutris runtime
    1. Select League of Legends in Lutris
    2. Select `Configure`
    3. Select `System options`
    4. Enable `Disable Lutris runtime`, then `Save`

!!! info "Optimal performance settings"
    You may need to adjust some settings for optimal performance once installed. Read more in [📈 How to Optimise League of Legends](../optimise/index.md).

!!! failure "Something not working?"
    Read more on common problems, how to solve them, and other troubleshooting tips in [🛠️ Troubleshooting and Technical Support](../troubleshooting/index.md).
