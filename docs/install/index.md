# ▶️ How to Install League of Legends

<img src="../assets/games/league_of_legends.png" alt="drawing" width="300"/>

!!! failure "League of Legends is no longer playable on Linux"
    Since patch [14.9](https://www.leagueoflegends.com/en-us/news/game-updates/patch-14-9-notes/), Vanguard has been activated.
    This means that League of Legends is no longer playable on Linux.

Before you start make sure you're running an up-to-date and currently supported distribution. If you are unsure if you are using an appropriate distribution, read [Which Distributions Can I Play On?](../faq/index.md/#which-distributions-can-i-play-on) If necessary run a package update, reboot, and continue with the rest of this guide.

There are several different methods of installing and playing League of Legends on Linux; this wiki covers the most popular ones.

For those who are unsure, or are otherwise new to Linux, the [Lutris](lutris.md) method is suggested. Lutris is a GUI game library frontend that aims to simplify managing Wine configurations for non-native games on Linux. It is one of the most commonly used methods, and [the Lutris wiki](https://github.com/lutris/lutris/wiki) is a high quality resource for troubleshooting and other information.

For those who choose not to use Lutris, it is still recommended to follow the steps outlined in the [Installing Drivers](https://github.com/lutris/docs/blob/master/InstallingDrivers.md) and [Wine Dependencies](https://github.com/lutris/docs/blob/master/WineDependencies.md) Lutris wiki pages as they cover all of the essential prerequisites required to run League of Legends on most systems.

Specific instructions can also be found for for Valve's [Steam Deck](steam_deck.md).

## [Lutris](lutris.md)

Universal game library and Wine manager for Linux. See also /r/Lutris  and the [Lutris Forums](https://forums.lutris.net/).

## [leagueoflegends-git](leagueoflegends_git.md)

Helper script for installing and running League of Legends on Linux maintained by /u/kyechou.

## [Snap](snap.md)

League of Legends container for Canonical's `snapd` packaging method, commonly found on Ubuntu-based distributions.

## [Other Methods](other.md)

Other ways to install that can be experimental, technically complex, come with heavy drawbacks, or are untested.

## [Steam Deck](steam_deck.md)

Step-by-step instructions for Valve's handheld Linux gaming device, the [Steam Deck](https://www.steamdeck.com/).

!!! info "Optimal performance settings"
    You may need to adjust some settings for optimal performance once installed. Read more in [📈 How to Optimise League of Legends](../optimise/index.md).

!!! failure "Something not working?"
    Read more on common problems, how to solve them, and other troubleshooting tips in [🛠️ Troubleshooting and Technical Support](../troubleshooting/index.md).