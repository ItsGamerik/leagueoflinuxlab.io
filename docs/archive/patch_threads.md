# Previous Patch Megathreads

Starting with the infamous patch 13.7 which broke League due to Riot's rollout of 32-bit deprecation, regular patch feedback threads have been posted to gather feedback and discuss the state of League on Linux for a given patch.

To view older patch megathreads, see below:

* [Patch 13.7](https://web.archive.org/web/20230405092758/https://old.reddit.com/r/leagueoflinux/comments/12cei7t/patch_137_feedback_megathread_transition_to_64bit/)
* [Patch 13.9](https://web.archive.org/web/20230518160347/https://old.reddit.com/r/leagueoflinux/comments/136yrpp/patch_139_megathread/)
* [Patch 13.10](https://web.archive.org/web/20230518115244/https://old.reddit.com/r/leagueoflinux/comments/13kv3v9/patch_1310_megathread/)
* [Patch 13.11](https://web.archive.org/web/20230601164528/https://old.reddit.com/r/leagueoflinux/comments/13xmllg/patch_1311_megathread/)
