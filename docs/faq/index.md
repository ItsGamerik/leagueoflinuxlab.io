# ❓ Frequently Asked Questions

Questions or topics frequently brought up on this subreddit and abroad.

!!! Failure "Vanguard anticheat has been implemented in League of Legends"
    Since patch [14.9](https://www.leagueoflegends.com/en-us/news/game-updates/patch-14-9-notes/), Vanguard has been activated. League of Legends will not be playable in the forseeable future

## [Vanguard Anticheat? What is it? Is it Coming to League?](vanguard.md)

Vanguard is a dense and controversial topic; it was one of the initial reasons this wiki was written in the first place! Given the unique amount of content and level of importance, it warrants its own [separate FAQ page](vanguard.md). In that page you can find details about what Vanguard is, as well as a comprehensive timeline of all public Riot statements and actions regarding anticheats and Linux players in general.

## Can I Get Permabanned for Playing on Linux?

No you will not get permabanned for playing League on Linux. While accidental bans are possible, they are *extremely* rare. Riot have stated that [playing League on Linux is not permaban-worthy](https://web.archive.org/web/20190410014937/https://boards.na.leagueoflegends.com/en/c/bug-report/GX3Zhxwe-game-client-anti-cheat-known-issues-and-fixes?comment=00020008) and they [actively try to make sure that their anticheat doesn’t immediately flag Wine users as cheaters](https://web.archive.org/web/20230317043038/https://old.reddit.com/r/leagueoflinux/comments/dfnq9e/riot_games/f380cwz/).

In mid July 2021 there was an accidental banwave that falsely flagged a large portion of the Wine gamers as cheaters, prompting bans. In the [relevant thread](https://old.reddit.com/r/leagueoflinux/comments/ogwoj8/apparent_accidental_ban_wave_thread/) Rioter /u/riotk3o kindly jumped in and reverted every ban shortly after the initial reports came in. **Every case of a user being falsely flagged was then reverted.**

While playing League on Linux is not officially supported, that is to say no native client or helpdesk support, Riot do not take any active measures against legitimate Linux users. 

Given that r/leagueoflinux is not affiliated with Riot, not only do commenters have no way to verify stories posted, but also even if we could, none of us have the powers to resolve any account-related issues. Therefore **posts regarding account bans will be removed and you will be directed to Riot support instead**. In exceptional cases, such as the example above, one thread will address the situation to not clutter the front page. 

r/leagueoflinux is not a Riot-affiliated subreddit, nor is it an official channel for account-related issues. All account-related requests and questions should be [sent to official Riot Support](https://support.riotgames.com/hc/en-us).

## Which Distributions can I Play On?

All major desktop distributions are more than capable of playing League via the instructions in [▶️ How to Install League of Legends](../install/index.md). This includes [Debian](https://www.debian.org/) and [Arch](https://archlinux.org/)-based distributions such as [Ubuntu](https://ubuntu.com/), [Pop!_OS](https://pop.system76.com/), [Manjaro](https://manjaro.org/), [elementary OS](https://elementary.io/), [Linux Mint](https://linuxmint.com/) etc. and other major distributions such as [Fedora](https://getfedora.org/) or [Gentoo](https://www.gentoo.org/). There are no major differences in performance between distros. In other words; all distributions perform equally on average, and other factors such as hardware configuration play a much larger role in performance than distribution choice.

If you find yourself asking this question, or are otherwise inexperienced with Linux, it is best to choose the latest Ubuntu LTS or one of it's derivatives due to it's general user-friendliness and vast online support. It is more common to run into issues on distributions that require a higher degree of technical knowledge such as Arch, Manjaro, Gentoo and Fedora, than it is the relatively stable Ubuntu LTS releases if you are inexperienced.

If you are on a rolling release distro make sure to stay updated (especially with Wine and display drivers). If you are on a point-release distro make sure to stay updated and use a currently supported version. Do not use a distro that is no longer supported or EOL (end of life). For example: at the time of writing Ubuntu LTS versions 22.04, 20.04 and 18.04 are supported whereas LTS versions 16.04 and below are no longer supported.

Do **not** attempt to run League on any distribution that is [not designed for desktop use](https://www.kali.org/docs/introduction/should-i-use-kali-linux/). Posts regarding support on such distros will be removed. This includes, but is not limited to: Kali Linux, Clear Linux, ClearOS, BlackArch and Pentoo.

Distribution recommendations for specific hardware configurations are better suited for /r/FindMeADistro rather than this subreddit. Such posts will be removed and you will be directed to /r/FindMeADistro and this FAQ article.

Additionally, although their PR and marketing implies that Garuda Linux is designed for gaming, it's **not** recommended for playing League on Linux; their development team is extremely small, the improvements they add over vanilla Arch or even Manjaro are negligible at best, and the configurations they deploy by default are known to not play well with League or some of the workarounds required to get League running properly. Support requests for Garuda Linux are allowed on the subreddit, but it is **heavily discouraged** to use. There are far more appropriate Arch-based distros that are better suited and more mature than Garuda.

## Third Party Programs: Overlays, Skin Changers and Other Mods


**⚠️ Disclaimer: use third party programs at your own risk. None of the mods listed below are endorsed or vetted by r/leagueoflinux or it's moderators. All information, reviews and documentation are provided by community members. The safety of your account and installation are not guaranteed!**

Riot's stance [per their own FAQ](https://support-leagueoflegends.riotgames.com/hc/en-us/articles/225266848-Third-Party-Applications) is:

> No software should interfere directly with the in-game player experience, from when you press “Play” to the end-of-game screen.

They then go on to state in the article:

> Competitive games work because each player is afforded the same amount of information about the game and other players. Third-party applications are programs or files that aren’t part of League of Legends. Applications we take issue with are those which impede the competitive nature of League of Legends.

> We don’t like applications that provide measurable player advantage. We’d like to set fair expectations by calling out some features that definitely aren’t okay. Some examples of measurable player advantage:

> * Exposing information that’s intentionally obfuscated (cooldowns or timers)
> * Taking actions on your behalf (botting or scripting)
> * Drawing conclusions for you (predicting enemy positions)
> * Altering your field of intelligence (zoomhacks or global ult alerts)

Specifically regarding skins:

> For now, custom models and artwork sit firmly in the “use-at-your-own-risk” category, but we’re keeping an eye on them. We can’t optimize for assets we don’t develop, and if they do become a concern for any reason in the future (ie. they provide measurable player advantage), we’ll take action based on the stance we just laid out.

> Gray areas aside, there are definitely applications which are just not okay, and while the number of players in this space are very low (think a fraction of a percentage), we remain vigilant and hand out punishments to those who show patterns of consistent abuse.

Therefore it is technically possible to run third party mods within the Wine contexts of games played on Linux. Given the nature of third party programs, you may or may not get adequate support in this subreddit if you encounter issues. It is likely best to first reach out to the developer of the program first regarding Wine support. However, that does not prohibit discussions or support requests in the subreddit.

To run `.exe` files inside a Lutris installation:

1. Select League of Legends in Lutris
2. Select the 🔺triangle next to Wine symbol
3. Select `Run EXE inside Wine prefix`
4. Choose your desired `.exe` file

**Popular HUDs and Rune Helpers:**

**✅ [ChampR](https://github.com/cangzhang/champ-r)**

* Originally [for Windows](https://github.com/cangzhang/champ-r), forked [for Linux natively](https://github.com/GeorgeV220/champ-r/) by community member /u/georgev222 
* For more information see [this thread](https://web.archive.org/web/20230317043038/https://old.reddit.com/r/leagueoflinux/comments/qz6v1u/champr_league_of_legends_helper_import_builds_and/)

**✅ [Blitz.gg](https://blitz.gg/)**

* For instructions see [this thread](https://web.archive.org/web/20230317042857/https://old.reddit.com/r/leagueoflinux/comments/pdekql/i_got_blitz_working_with_the_new_615_patched_wine/) written by community member /u/mohad12211 

**[❓ RuneChanger](https://runechanger.stirante.com/)**

* Potentially works, needs documentation
* Please reach out to /u/TheAcenomad if you have reports of RuneChanger performance

**✅ [RuneBook](https://github.com/Soundofdarkness/RuneBook)**

* For instructions see [this thread](https://web.archive.org/web/20230103055740/https://old.reddit.com/r/leagueoflinux/comments/n5s2rs/auto_runes_compatible_with_leagueoflegendsgit/) 

**✅ [OP.GG Chrome extension run in Wine](https://op.gg/)**

* For instructions see [this thread](https://web.archive.org/web/20230103055740/https://old.reddit.com/r/leagueoflinux/comments/n5s2rs/auto_runes_compatible_with_leagueoflegendsgit/) 

_Editor's note: I am not familiar enough with third party programs, nor do I intend on ever using them. If changes need to be made to the above please reach out to /u/TheAcenomad. Last major update: 23/11/2021._

## Why Can't We Play on Upstream Wine?

Beginning in Wine version 5.10 upstream Wine introduced a number of changes that [broke compatibility with League](https://bugs.winehq.org/show_bug.cgi?id=49373). While there have been some [Wine updates](https://web.archive.org/web/20230317042857/https://old.reddit.com/r/linux_gaming/comments/nse47b/wine_610_released/) that referenced fixes League of Legends, none of them have [directly addressed](https://bugs.winehq.org/show_bug.cgi?id=47198#c105) the issues that block the game from running.

Wine versions based on Wine 6.0 and above that are correctly patched with [this patch](https://bugs.winehq.org/attachment.cgi?id=70550&action=diff&context=patch&collapsed=&headers=1&format=raw) and [this patch](https://bugs.winehq.org/attachment.cgi?id=70530&action=diff&context=patch&collapsed=&headers=1&format=raw) can run both the League client and game which is why currently compatible Wine versions such as [GloriousEggroll's 6.16 Lutris-GE-6.16-LoL](https://github.com/GloriousEggroll/wine-ge-custom/releases/tag/6.16-GE-LoL) are compatible and recommended.

Community member /u/kassindornelles had a casual conversation with a Wine dev and [posted their chat in a thread](https://web.archive.org/web/20230317042857/https://old.reddit.com/r/leagueoflinux/comments/pnmy1u/i_had_a_conversation_with_a_wine_dev_about_lol/). While not an official statement from WineHQ, they did confirm that League compatibility with upstream was being worked on but will likely be projected for after Wine 7.0 release:

> I think the goal has been 7.0 but I doubt it will be done by then [...] maybe by then there will be a way to manually enable that pathbut for anything to work we really need work on winevulkan, opengl32, and winex11 

## Can I Play on Wayland/Hyperland?

Yes! Most distros that ship Wayland by default don't need to do anything special aside from following the instructions in [▶️ How to Install League of Legends](../install/index.md). 

Depending on how your system is configured you may need to consult Wayland's [XWayland documentation](https://wayland.freedesktop.org/xserver.html).

If you experience tiny or invisible windows using Hyperland, you may need to enforce some specific window rules in order for the clients to appear. Steps can be found in the [relevant troubleshooting article](../troubleshooting/solutions.md#wiki_.2705_tiny_or_invisible_client_windows_on_hyperland).

## How Can I Support League on Linux?

Since r/leagueoflinux is maintained by volunteers, much of the work is collaborative amongst many community members. If you would like to donate to a particular developer or individual, please consult their individual repository or homepage. Currently r/leagueoflinux does not accept donations as an organisation of any kind. If you would like to support the subreddit, please post, discuss, upvote and spread the wonderful world of open source gaming to others!

## Does Wine Make Me Lag? My Ping is Higher on Linux?

While translation layers inherently add some level of additional overhead, in most cases it is extremely negligible. Playing via Wine will not introduce any perceptible network latency. If you notice that your ping is higher on Linux compared to Windows, you likely have issues elsewhere in your networking stack.

Ensure your system is completely up-to-date, particularly any necessary networking drivers. It is recommended where possible, regardless of operating system, to play multiplayer games from a wired Ethernet connection instead of a wireless connection. If you require help, feel free to [submit a support request post](../troubleshooting/requests.md).

## Does r/leagueoflinux Have a Discord?

Currently there are no Discords affiliated with r/leagueoflinux or its moderation team.

For those who still wish to use the service, there are Discords for various Linux gaming and League-related subjects, a non-comprehensive list can be found below:

* [Lutris](https://discordapp.com/invite/Pnt5CuY)
* [GloriousEggroll](https://discord.gg/6y3BdzC)
* [League of Legends](https://discord.com/invite/leagueoflegends)
* [GamingOnLinux](https://discord.com/invite/AghnYbMjYg)
* [Linux Gamers Group](https://discord.gg/BaWqd4r)
* [Linux Gaming](https://discord.com/invite/linuxgaming)
* [LEC](https://discord.com/invite/lec-official)
