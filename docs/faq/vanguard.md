!!! Failure "Vanguard anticheat is coming to League of Legends, the era of League of Linux is over"
    :material-youtube: [Riot announcement video (Vanguard @ 12:12)](https://youtu.be/9U_jEzKf0_0?t=732)
    
    :material-reddit: [Farewell r/leagueoflinux: Vanguard is coming to League of Legends, likely ending the era of League of Linux](https://old.reddit.com/r/leagueoflinux/comments/1abhx9f/farewell_rleagueoflinux_vanguard_is_coming_to/)

    :material-reddit: [Collection of Rioter comments](https://old.reddit.com/r/leagueoflegends/comments/18zebss/what_do_you_guys_think_of_vangaurd/kgi2i6j/)

    :material-steam: [DotA2 Steam page](https://store.steampowered.com/app/570/Dota_2/)

The personal conjecture in this page has been proven false. Nonetheless, the historical information serves archival purposes so the content below has not been heavily modified aside from striking out now incorrect information.

# Vanguard Anticheat? What is it? Is it Coming to League?

## What is Vanguard?

[Vanguard](https://support-valorant.riotgames.com/hc/en-us/articles/360046160933-What-is-Vanguard-) is the proprietary, closed-source, ring0, kernel-level anticheat implemented exclusively in VALORANT which is currently incompatible with Wine and virtual machines, therefore Linux-based gaming as a whole. 


## Rumours and Online Discourse

~~Over the years there have been a number of online commenters claim that Riot will implement Vanguard in League of Legends in 2021, 2022, 2023. Most importantly, all of these claims have lacked concrete citations from any official source that confirms Vanguards implementation in League of Legends at all, let alone any form of timeline. **At this time there has not been any official announcement confirming the implementation of Vanguard in League of Legends.**~~

## Future Implementation

~~While it is indeed possible that Vanguard will someday be implemented in League of Legends, we do not have any reason to believe that it is currently planned. The threat of an anticheat cutting off access to a given game on Linux is unfortunately relevant to the majority of games in the market since there is an inherent level of uncertainty regarding the future playability for many games on Linux. Players are still historically more at risk of [Epic Games pulling Linux support](https://www.gamingonlinux.com/2020/01/psyonix-are-ending-support-for-rocket-league-on-both-linux-and-macos/comment_id=173046) than Riot Games. To [quote Riot Perma](hhttps://web.archive.org/web/20230610145724/https://old.reddit.com/r/leagueoflegends/comments/8sbes0/gnulinux_compatibility_riot_restores_gpu/):~~

> We have not consciously prevented the use of Wine for playing the game

~~Despite lack of official support, Riots developers have taken a Wine-friendly approach to League of Legends and do not actively hinder, ban or block Wine users from playing. Because of this it is safe to assume that we will not have to deal with Vanguard in League for at least a _very_ long time.~~

~~More generally, the gaming industry has been trending favourably towards Linux gamers in recent years with the success of the Linux-based [Steam Deck](https://www.steamdeck.com/) from Valve, and many similar anticheats, such as EAC and BattlEye, gaining Linux support to various degrees. While this is not concrete evidence of anything Riot will do, it does at least inspire confidence that Riots competitors are taking the space seriously.~~

## Timeline of Events

Below you can find a comprehensive list of every occurrence of Riot or a Rioter's statements and actions regarding Linux/Wine players with links to original sources.

### June 2018: [Official League forums post confirming GPU-passthrough and Wine compatibility](https://web.archive.org/web/20190410014937/https://boards.na.leagueoflegends.com/en/c/bug-report/GX3Zhxwe-game-client-anti-cheat-known-issues-and-fixes?comment=00020008)

In a League forums post (now deprecated RIP, still accessible with [Wayback Machine](https://web.archive.org/web/20190410014937/https://boards.na.leagueoflegends.com/en/c/bug-report/GX3Zhxwe-game-client-anti-cheat-known-issues-and-fixes?comment=00020008)), Riot Perma confirmed that Riot recognize, monitor and actively allow players who use Wine and GPU-passthrough virtualisation. r/leagueoflinx mod u/EnglishDentist also crossposted this forum post to the main r/leagueoflegends subreddit [here](https://web.archive.org/web/20230317042857/https://old.reddit.com/r/leagueoflegends/comments/8sbes0/gnulinux_compatibility_riot_restores_gpu/). Riot Perma's full comment is also archieved in that thread:

> For players using Wine, the community is already at work fixing incompatibilities with our changes. These changes were live on our Public Beta Environment for several months to give developers of third-party applications time to adapt. As a normal part of software development, especially with such comprehensive changes, there can be issues introduced with third party applications. **We have not consciously prevented the use of Wine for playing the game**, and we ask for your patience as the Wine community remedies any incompatibilities.

### Early 2020: [Official Riot /dev/null article on anticheat](https://na.leagueoflegends.com/en-pl/news/dev/dev-null-anti-cheat-kernel-driver/)

This was the initial article on Vanguard from Riot. There is only one reference to the fact that it has the potential to come to League and it does *not* give a confirmation or a timeline, only a reference to Vanguard's *potential* to be implemented in League after VALORANT (*Project A*, at the time): 

> Disclaimer: This post is kinda tech-heavy and concerns anti-cheat tooling that won’t be exclusive to League of Legends. Other games (like Project A) will be protected by the referenced upgrade before LoL is.

### Feb 04 2020: [Rioter gave us a glimpse of internal Linux player data](https://web.archive.org/web/20230317042857/ttps://old.reddit.com/r/leagueoflegends/comments/eybl03/new_anticheat_system_in_lol_and_other_upcoming/fghrauz/?context=10000)

The Rioter who wrote the above /dev/null article, /u/mirageofpenguins, shared supposed internal information from Linux players, "~6000 games a day played by ~700 Wine users" (note: this was posted in Feb of 2020 so numbers have most likely shifted since then), as well as their personal opinion on kernel-level anticheat. Full thread includes more info, however a notable highlight is:

> "So, the driver component wouldn't be on LoL for at least a year (if we even decided to utilize it)"

### Apr 17 2020: [Riot publishes a response to the Vanguard privacy backlash](https://www.riotgames.com/en/news/a-message-about-vanguard-from-our-security-privacy-teams)

The article did not give us any new information regarding Vanguard's potential future in League, nor regarding Linux/MacOS players. However, it's entry is included for documentation's sake.

### Sep 27 2020: [Lead Designer of TFT confirms Riot have no plans to bring Vanguard to TFT](https://clips.twitch.tv/ArbitraryTransparentWatermelonCclamChamp)

While not entirely apples-to-apples, TFT and League of Legends are the only two games to share the main League client and are more closely connected than any of Riot's other IPs. See the [related subreddit thread and discussion here.](https://old.reddit.com/r/leagueoflinux/comments/j0v2hp/mortdog_pretty_much_confirms_that_riot_vanguard/) In response to the question "Any information on if Riot Vanguard will be implemented for TFT?", he answers:

> "I don't believe so, I would be shocked if we did that."

### May 01 2021: [Riot sends Privacy Policy and TOS update email](https://web.archive.org/web/20230103055739/https://old.reddit.com/r/leagueoflinux/comments/n35eu8/updated_wording_on_vanguard_anticheat_in_recent/)

The email covered various other topics unrelated to Vanguard such as a new refund policy and analyzing voice chat data (!), however there was a short paragraph where Riot discuss Vanguard. The email only confirmed two points: Riot want to leave the door open in their leagalese for future implementation in other titles and if they decide to implement it in a given title they will annouce so far in advance (as they have done with the majority of tech updates). [Full thread here](https://web.archive.org/web/20230103055739/https://old.reddit.com/r/leagueoflinux/comments/n35eu8/updated_wording_on_vanguard_anticheat_in_recent/).

> ... we updated the language about anti-cheat software that we may require you to install to play certain instances of our games. We will always be as transparent as possible about our anti-cheat programs without compromising our solutions. We’ll let you know about any new anti-cheat programs especially with kernel mode drivers well before they’re released and required to be installed.

### July 2021: [Accidental banwave and subsquent unbanning by a Rioter](https://web.archive.org/web/20230317043038/https://old.reddit.com/r/leagueoflinux/comments/ogwoj8/apparent_accidental_ban_wave_thread/)

An apparent accidental banwave that falsely flagged a large portion of the Wine gamers as cheaters, prompting bans. In the [relevant thread](hhttps://web.archive.org/web/20230317043038/https://old.reddit.com/r/leagueoflinux/comments/ogwoj8/apparent_accidental_ban_wave_thread/) Rioter /u/riotk3o kindly jumped in and reverted every ban shortly after the initial reports came in. **Every case of a user being falsely flagged was then reverted.**

### Jan 13 2023: [Brief mention of anticheat updates in a Twitter video](https://twitter.com/LeagueOfLegends/status/1614028829823209472)

Riot Meddler confirmed in [a Twitter video](https://twitter.com/LeagueOfLegends/status/1614028829823209472) that there will indeed be changes coming to a large portion of League's backend tech in 2023, including League's anticheat. However, in typical Riot fashion the wording, details, and projected timeline are rather vague: the anticheat is only give a single sentence worth in the video (5:53).

> ... and how our anticheat works; updating that...


Additional Riot resources regarding Vanguard that don't have any implications for Linux/Wine users:

* [Evolution of Security at Riot](https://technology.riotgames.com/news/evolution-security-riot)
* [A Message about Vanguard from our Privacy & Security Teams](https://www.riotgames.com/en/news/a-message-about-vanguard-from-our-security-privacy-teams)
* [What is Vanguard?](https://support-valorant.riotgames.com/hc/en-us/articles/360046160933-What-is-Vanguard-)
