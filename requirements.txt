# Documentation static site generator & deployment tool
mkdocs>=1.5.3
mkdocs-material>=9.4.8
Jinja2>=3.1.2

# Add your custom theme if not inside a theme_dir
# (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
#  mkdocs-material>=5.4.0
